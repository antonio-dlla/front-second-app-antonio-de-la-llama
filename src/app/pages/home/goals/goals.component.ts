import { Component, Input, OnInit } from '@angular/core';
import {Goals} from '../models/Ihome'; 



@Component({
  selector: 'app-goals',
  templateUrl: './goals.component.html',
  styleUrls: ['./goals.component.scss']
})
export class GoalsComponent implements OnInit {

  @Input() public goals!: Goals;
 



  constructor() { }

  ngOnInit(): void {
  }

}
