import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Hero } from '../models/Ihome';




@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})


export class HeroComponent implements OnInit {
  
  @Input() public hero!: Hero; 
  @Output() passInfo = new EventEmitter<string>();

  changeStuff(){
    this.hero.name = 'colega';
    this.passInfo.emit(this.hero.name);
  }

  constructor() { }

  ngOnInit(): void {
  }
  
}
