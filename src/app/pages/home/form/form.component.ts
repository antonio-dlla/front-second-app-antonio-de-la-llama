import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Form } from '../models/Ihome';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  public signUpForm: FormGroup = undefined;
  public submitted: boolean = false;
  @Output() public newUserEmmit = new EventEmitter <Form>();

  constructor(private formBuilder: FormBuilder) {
    this.signUpForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(1)]],
      email: ['', [Validators.required, Validators.email]],
      age: ['', [Validators.required, Validators.min(16)]],
      message: ['', [Validators.maxLength(300)]],
    })
   }

  ngOnInit(): void {
  }

  public onSubmit(): void{
    this.submitted = true;
    if(this.signUpForm.valid) {
      const registeredUser: Form = {
        name: this.signUpForm.get('name')?.value,
        email: this.signUpForm.get('email')?.value,
        age: this.signUpForm.get('age')?.value,
        message: this.signUpForm.get('message')?.value,
      };
      this.newUserData(registeredUser);
      console.log(registeredUser);
      this.signUpForm.reset();
      this.submitted = false;
    }
  }
  public newUserData(registeredUser: Form) {
    this.newUserEmmit.emit(registeredUser);
  } 
}
