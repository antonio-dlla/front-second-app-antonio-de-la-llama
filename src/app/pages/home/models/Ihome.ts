export interface Hero {
    img: Img;
    title: string;
    name: string;
    description: string;
};

export interface Img {
    url: string;
    alt: string;
};

export interface Goals {
    title: string;
    messages: string[];
};

export interface Form {
    name: string;
    email: string;
    age: number;
    message: string;
} 