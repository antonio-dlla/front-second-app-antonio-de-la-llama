import { Component, OnInit } from '@angular/core';
import { Hero, Goals, Form } from './models/Ihome' ;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public hero: Hero;
  public goals: Goals;
  public form: Form = null;
  

  constructor() {
    this.hero = {
      img: {
        url: 'https://comextic.com/web/wp-content/uploads/2018/07/840099-background-space-2560x1600-photos.jpg',
        alt: 'background-universe',
        }
      ,
      title: '¡Hola!',
      name: 'contempla la potencia de Angular',
      description: 'Ésta es una nueva página, creada por mí, Antonio de la Llama, con Angular. Puede que si no sabes de desarrollo web pienses que es una mierda, pero créeme, ha costado lo suyo.'
    };
    
    this.goals = {
      title: 'Para hacer esta página, he tenido que:',
      messages: ['conocer la relación entre padres e hijos',
      'usar inputs y outputs para el intercambio de información',
      'usar ngIf y ngClass','crear un formulario reactivo']

    };


   }

  ngOnInit(): void {
  }

  public subscribedUser(form: Form){
    this.form= form;
  }

}
